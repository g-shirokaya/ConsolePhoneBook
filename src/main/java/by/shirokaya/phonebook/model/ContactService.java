package by.shirokaya.phonebook.model;

import by.shirokaya.phonebook.model.obj.Contact;
import org.apache.log4j.NDC;

import static by.shirokaya.phonebook.model.obj.Contact.NumberType;

class ContactService extends PhoneBookService {

    void updateFirstName(Contact contact, String firstName) {
        NDC.push("Update firstName on " + firstName);
        logger.debug("Old: [{}]", contact);
        delete(contact);
        contact.setFirstName(firstName);
        update(contact);
    }

    void updateLastName(Contact contact, String lastName) {
        NDC.push("Update lastName on " + lastName);
        logger.debug("Old: [{}]", contact);
        delete(contact);
        contact.setLastName(lastName);
        update(contact);
    }

    void updateNumber(Contact contact, String number) {
        NDC.push("Update number on " + number);
        logger.debug("Old: [{}]", contact);
        delete(contact);
        contact.setNumber(number);
        update(contact);
    }

    void updateNumberType(Contact contact, NumberType type) {
        NDC.push("Update numberType on " + type);
        logger.debug("Old: [{}]", contact);
        delete(contact);
        contact.setNumberType(type);
        update(contact);
    }

}
