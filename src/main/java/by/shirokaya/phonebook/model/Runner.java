package by.shirokaya.phonebook.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Runner {

    private static Logger logger = LoggerFactory.getLogger(Runner.class);

    public static void main(String[] args) {

        logger.info("Entering app");
        Manager manager = new Manager();
        manager.goToMainPage();

    }
}
