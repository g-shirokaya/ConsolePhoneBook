package by.shirokaya.phonebook.model;

import by.shirokaya.phonebook.model.obj.Contact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings(value = "unchecked")
final class IOService {

    private static Logger logger = LoggerFactory.getLogger(IOService.class);
    private File file;

    {
        file = new FileService().getFile();
    }

    void save(List<Contact> contacts) {

        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(contacts);
            objectOutputStream.close();
        } catch (EOFException e) {
            logger.debug("File is empty");
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Saving failed cause of exception", e);
        }
    }

    List<Contact> read() {

        List<Contact> phoneBook = new ArrayList<>();
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            phoneBook = (List<Contact>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (EOFException ex) {
            logger.debug("File is empty");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("Reading failed cause of exception", e);
        }
        return phoneBook;
    }

    private static class FileService {
        private String fileSeparator = System.getProperty("file.separator");
        private final String PATH_NAME = "./.phonebook/PhoneBook.book".replace("/", fileSeparator);
        private File file = new File(PATH_NAME);

        private File getFile() {
            try {
                if (file.createNewFile()) {
                    return file;
                }
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("File getting failed cause of exception", e);
            }
            return file;
        }
    }

}
