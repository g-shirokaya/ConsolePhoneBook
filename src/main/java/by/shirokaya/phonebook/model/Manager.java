package by.shirokaya.phonebook.model;

import by.shirokaya.phonebook.model.obj.Contact;
import by.shirokaya.phonebook.view.ConsoleManager;
import by.shirokaya.phonebook.view.PhoneBookBot;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@SuppressWarnings("InfiniteLoopStatement")
class Manager implements PhoneBookBot {

    private ConsoleManager consoleManager;
    private ContactService contactService;
    private Contact currentContact;
    private static Logger logger = LoggerFactory.getLogger(Manager.class);

    {
        consoleManager = new ConsoleManager();
        contactService = new ContactService();
        currentContact = new Contact();
    }

    void goToMainPage() {

        logger.trace("Went to 'main' page");
        while (true) {
            showMainPage();

            int page = consoleManager.getIntegerFromConsole();
            switch (page) {
                case 1:
                    showPhoneBook();
                    break;
                case 2:
                    addNewContact();
                    break;
                case 3:
                    goToSearchBy();
                    break;
                case 4:
                    close();
                    break;
                default:
                    sayTryAgain();
            }
        }
    }

    private void goToSearchBy() {

        logger.trace("Went to 'search' page");
        while (true) {
            saySearchBy();

            int page = consoleManager.getIntegerFromConsole();
            switch (page) {
                case 1:
                    searchByFirstName();
                    break;
                case 2:
                    searchByPartOfFirstName();
                    break;
                case 3:
                    goToMainPage();
                    break;
                default:
                    sayTryAgain();
            }
        }
    }

    private void searchByPartOfFirstName() {
        logger.trace("Went to 'search by part of name' page");
        askPartOfName();
        String partOfName = consoleManager.getLineFromConsole();
        List<Contact> searchResult = contactService.findByPartOfWord(partOfName);
        if (searchResult.isEmpty()) {
            sayNothingWasFound();
            goToSearchBy();
        }
        sayWasFound();

        int number = 0;
        for (Contact contact : searchResult) {
            number++;
            printNumber(number);
            contactService.read(contact);
        }
        int index = chooseContact(number);
        currentContact = searchResult.get(index);
        logger.trace("Was chosen: [{}]", currentContact);
        showOptions();
    }

    private int chooseContact(int maxNumber) {
        sayMayGetBack();
        sayChooseContact();
        while (true) {
            int choice = consoleManager.getIntegerFromConsole();
            if (choice > maxNumber || choice < 0) {
                sayTryAgain();
            } else if (choice == 0) {
                goToSearchBy();
            } else return choice - 1;
        }
    }

    private void searchByFirstName() {
        logger.trace("Went to 'search by first name' page");
        String name = consoleManager.getValidFirstName();
        Contact searchResult = contactService.findByUniqueId(name);
        if (searchResult == null) {
            sayNothingWasFound();
            goToSearchBy();
            return;
        }
        currentContact = searchResult;
        logger.trace("Was chosen: [{}]", currentContact);
        contactService.read(currentContact);
        showOptions();
    }

    private void showOptions() {
        logger.trace("Went to 'show options' page");
        while (true) {
            saySelectOption();

            int page = consoleManager.getIntegerFromConsole();
            switch (page) {
                case 1:
                    updateFirstName();
                    break;
                case 2:
                    updateLastName();
                    break;
                case 3:
                    updateNumber();
                    break;
                case 4:
                    updateNumberType();
                    break;
                case 5:
                    deleteContact();
                    break;
                case 6:
                    goToSearchBy();
                    break;
                case 861:
                    clearPhoneBook();
                    break;
                default:
                    sayTryAgain();
            }
        }
    }

    private void clearPhoneBook() {
        logger.trace("Went to 'clear phoneBook' page");
        if (contactService.isEmpty()) {
            sayIsEmpty();
            return;
        }
        askConfirmToDeleteAllContacts();
        confirmDeleting("ALL");
    }

    private void deleteAllOrOne(String option) {
        if (option == null) {
            sayTryAgain();
        } else if (option.equals("ALL")) {
            contactService.deleteAll();
            sayIsEmpty();
        } else if (option.equals("ONE")) {
            contactService.delete(currentContact);
            sayDeleted();
        }
    }

    private void confirmDeleting(String option) {

        while (true) {
            int choice = consoleManager.getIntegerFromConsole();
            switch (choice) {
                case 1:
                    deleteAllOrOne(option);
                    logger.debug("Was deleted: [{}]", currentContact);
                    goToMainPage();
                    break;
                case 2:
                    showOptions();
                    break;
                default:
                    sayTryAgain();
            }
        }
    }

    private void deleteContact() {
        logger.trace("Went to 'delete contact' page");
        askConfirmToDeleteThis(currentContact.getFirstName());
        confirmDeleting("ONE");
    }

    private void updateNumberType() {
        logger.trace("Went to 'update number type' page");
        Contact.NumberType type = consoleManager.getTypeToUpdate();
        contactService.updateNumberType(currentContact, type);
        saySaved();
    }

    private void updateNumber() {
        logger.trace("Went to 'update number' page");
        String number = consoleManager.getValidNumber();
        contactService.updateNumber(currentContact, number);
        saySaved();
    }

    private void updateLastName() {
        logger.trace("Went to 'update last name' page");
        String name = consoleManager.getValidLastName();
        contactService.updateLastName(currentContact, name);
        saySaved();
    }

    private void updateFirstName() {
        logger.trace("Went to 'update first name' page");
        String name = consoleManager.getValidFirstName();
        while (contactService.exists(name)) {
            sayExists();
            name = consoleManager.getValidFirstName();
        }
        contactService.updateFirstName(currentContact, name);
        saySaved();
    }

    private void addNewContact() {
        logger.trace("Went to 'add new contact' page");
        currentContact = consoleManager.getNewContactFromConsole();
        while (contactService.exists(currentContact.getFirstName())) {
            sayExists();
            logger.trace("First name already exists: [{}]", currentContact.getFirstName());
            String newName = consoleManager.getValidFirstName();
            currentContact.setFirstName(newName);
        }
        NDC.push("Add new contact with first name " + currentContact.getFirstName());
        contactService.update(currentContact);
        saySaved();
        goToMainPage();
    }

    private void showPhoneBook() {
        logger.trace("Went to 'show phoneBook' page");
        if (contactService.isEmpty()) {
            sayIsEmpty();
            return;
        }
        contactService.readAll();
        if (consoleManager.getBack()) {
            goToMainPage();
        }
    }

    private void close() {
        logger.info("Exit app");
        System.exit(0);
    }


}
