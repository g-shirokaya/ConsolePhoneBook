package by.shirokaya.phonebook.model;

import by.shirokaya.phonebook.model.obj.Contact;
import by.shirokaya.phonebook.services.CRUDService;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;

public class PhoneBookService implements CRUDService<Contact, String> {

    private List<Contact> buffer;
    private IOService ioService;
    static Logger logger = LoggerFactory.getLogger(PhoneBookService.class);

    {
        buffer = new ArrayList<>();
        ioService = new IOService();
    }

    @Override
    public void read(Contact contact) {
        System.out.println(contact);
    }

    @Override
    public void readAll() {
        buffer = findAll();
        buffer.forEach(System.out :: println);
    }

    @Override
    public List<Contact> findAll() {
        return ioService.read();
    }

    boolean isEmpty() {
        return findAll().isEmpty();
    }

    @Override
    public Contact findByUniqueId(String firstName) {

        buffer = findAll();
        for (Contact contact : buffer) {
            if (contact.getFirstName().equalsIgnoreCase(firstName)) {
                return contact;
            }
        }
        return null;
    }

    boolean exists(String firstName) {
        buffer = findAll();
        for (Contact contact : buffer) {
            if (contact.getFirstName().equals(firstName)) {
                return true;
            }
        }
        return false;
    }

    List<Contact> findByPartOfWord(String partOfName) {
        List<Contact> searchResult = new ArrayList<>();
        if (partOfName == null) {
            return searchResult;
        }
        buffer = findAll();
        String part = partOfName.trim().toLowerCase();

        for (Contact contact : buffer) {
            if (contact.getFirstName().toLowerCase().contains(part)) {
                searchResult.add(contact);
            }
        }
        return searchResult;
    }

    @Override
    public void delete(Contact contact) {
        buffer = findAll();
        buffer.remove(contact);
        update();
    }

    @Override
    public void deleteAll() {
        buffer.clear();
        update();
        logger.debug("PhoneBook was cleared");
    }

    @Override
    public void update() {
        ioService.save(buffer);
    }

    @Override
    public void update(Contact contact) {
        buffer.add(contact);
        logger.debug("New: [{}]", contact);
        update();
        NDC.pop();
        NDC.remove();
    }
}
