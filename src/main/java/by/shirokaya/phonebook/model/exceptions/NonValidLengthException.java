package by.shirokaya.phonebook.model.exceptions;

public class NonValidLengthException extends Exception {

    public NonValidLengthException(String message) {
        super(message);
    }
}
