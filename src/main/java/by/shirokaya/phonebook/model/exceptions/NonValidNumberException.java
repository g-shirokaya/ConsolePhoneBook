package by.shirokaya.phonebook.model.exceptions;

public class NonValidNumberException extends Exception {

    public NonValidNumberException(String message) {
        super(message);
    }
}
