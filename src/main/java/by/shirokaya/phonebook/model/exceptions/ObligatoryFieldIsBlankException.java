package by.shirokaya.phonebook.model.exceptions;

public class ObligatoryFieldIsBlankException extends Exception {

    public ObligatoryFieldIsBlankException(String message) {
        super(message);
    }
}
