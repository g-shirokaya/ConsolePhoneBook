package by.shirokaya.phonebook.model.obj;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class Contact implements Serializable {

    private String firstName;
    private String lastName;
    private String number;
    private NumberType numberType;
    private Date creatingDate;

    public Contact() {
    }

    public Contact(String firstName, String lastName, String number, NumberType numberType) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.number = number;
        this.numberType = numberType;
        this.creatingDate = new Date();
    }

    public enum NumberType {
        MOBILE,
        HOME,
        WORK
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return Objects.equals(firstName, contact.firstName) &&
                Objects.equals(lastName, contact.lastName) &&
                Objects.equals(number, contact.number) &&
                numberType == contact.numberType &&
                Objects.equals(creatingDate, contact.creatingDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, number, numberType, creatingDate);
    }

    @Override
    public String toString() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd.MM.yyyy 'at' HH:mm:ss", Locale.US);
        return "FirstName: " + firstName +
                " \nLastName: " + lastName +
                " \nNumber: " + number +
                " \nNumberType: " + numberType +
                " \nCreated: " + dateFormat.format(creatingDate) + "\n";
    }


}


