package by.shirokaya.phonebook.services;

public interface CRUDService<T, ID> {

    void read (T t);

    void readAll();

    Iterable <T> findAll();

    T findByUniqueId(ID id);

    void delete(T t);

    void deleteAll();

    void update();

    void update (T t);

}
