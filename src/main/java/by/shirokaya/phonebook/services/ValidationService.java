package by.shirokaya.phonebook.services;

import by.shirokaya.phonebook.model.exceptions.*;

public class ValidationService {

    private static final String MATCHER = "^(\\s*)?(\\+)?([-+ ()]?\\d[-+ ()]?){7,15}(\\s*)?$";

    public static boolean firstNameIsValid(String firstName) throws ObligatoryFieldIsBlankException, NonValidLengthException {
        if (firstName.isBlank()) {
            throw new ObligatoryFieldIsBlankException("Validation -> Obligatory field is blank");
        }
        if (firstName.length() > 25) {
            throw new NonValidLengthException("Validation-> 25 symbols maximum.");
        }

        return true;
    }

    public static boolean lastNameIsValid(String lastName) throws NonValidLengthException {
        if (lastName.length() > 25) {
            throw new NonValidLengthException("Validation-> 25 symbols maximum.");
        }

        return true;
    }

    public static boolean numberIsValid(String number) throws NonValidNumberException, ObligatoryFieldIsBlankException {
        if (number.isBlank()) {
            throw new ObligatoryFieldIsBlankException("Validation -> Obligatory field is blank.");
        }
        if (!number.matches(MATCHER)) {
            throw new NonValidNumberException("Validation -> Non valid number");
        }

        return true;
    }
}
