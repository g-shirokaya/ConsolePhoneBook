package by.shirokaya.phonebook.view;

public interface PhoneBookBot {

    default void sayTryAgain() {
        System.out.println("Try again.");
    }

    default void showNumberTypes() {
        System.out.println("Select number type:\n1) MOBILE\n2) HOME\n3) WORK");
    }

    default void saySkip() {
        System.out.println("Press 0 to skip this step.");
    }

    default void askName() {
        System.out.println("Enter first name:");
    }

    default void askLastName() {
        System.out.println("Enter last name.\nOr press Space to keep it blank:");
    }

    default void askNumber() {
        System.out.println("Enter number:");
    }

    default void showMainPage() {
        System.out.println("Main menu:\n1) Show all contacts\n2) Add new contact\n3) Search by...\n4) Close");
    }

    default void sayMayGetBack() {
        System.out.println("Press 0 to get back.");
    }

    default void saySearchBy() {
        System.out.println("Select search criteria:\n1) By first name\n2) By part of first name\n3) Back to main menu");
    }

    default void saySelectOption() {
        System.out.println("Select option:\n1) Update first name\n2) Update last name\n3) Update number\n4) Update number type\n5) Delete contact\n6) Back to search\n861) Clear PhoneBook");
    }

    default void sayNothingWasFound() {
        System.out.println("Nothing was found.");
    }

    default void sayWasFound() {
        System.out.println("These were found: ");
    }

    default void sayChooseContact() {
        System.out.println("Or enter number of contact to see options: ");
    }

    default void printNumber(int number) {
        System.out.print(number + ") ");
    }

    default void askPartOfName() {
        System.out.println("Enter part of first name.\nOr press Enter to get all.");
    }

    default void askConfirmToDeleteAllContacts() {
        System.out.println("Are you sure you want to delete all contacts?\n1) Yes\n2) No");
    }
    default void askConfirmToDeleteThis(String firstName) {
        System.out.println("Are you sure you want to delete contact '" + firstName + "'?\n1) Yes\n2) No");
    }

    default void sayIsEmpty() {
        System.out.println("PhoneBook is empty.");
    }

    default void saySaved() {
        System.out.println("Saved.");
    }

    default void sayDeleted() {
        System.out.println("Deleted.");
    }

    default void sayExists() {
        System.out.println("This first name already exists.");
    }

}
