package by.shirokaya.phonebook.view;

import by.shirokaya.phonebook.model.exceptions.*;
import by.shirokaya.phonebook.model.obj.Contact;
import by.shirokaya.phonebook.services.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static by.shirokaya.phonebook.model.obj.Contact.NumberType;

import java.util.Scanner;
import java.util.function.Predicate;

public class ConsoleManager implements PhoneBookBot {

    private static final Scanner scanner = new Scanner(System.in);
    private static Logger logger = LoggerFactory.getLogger(ConsoleManager.class);

    public int getIntegerFromConsole() {

        while (!scanner.hasNextInt()) {
            sayTryAgain();
            scanner.next();
        }
        return scanner.nextInt();
    }

    public String getLineFromConsole() {
        String input = scanner.nextLine();

        if (input.isEmpty()) {
            input = scanner.nextLine();
        }
        return input;
    }

    private String getLineFromConsole(Predicate<String> validate) {

        String input = scanner.nextLine();

        if (input.isEmpty()) {
            input = scanner.nextLine();
        }
        while (!validate.test(input)) {
            sayTryAgain();
            input = scanner.nextLine();
        }
        return input;
    }

    public NumberType getTypeToUpdate() {
        showNumberTypes();
        saySkip();
        while (true) {

            int choice = getIntegerFromConsole();
            switch (choice) {
                case 0:
                case 1:
                    return NumberType.MOBILE;
                case 2:
                    return NumberType.HOME;
                case 3:
                    return NumberType.WORK;
                default:
                    sayTryAgain();
            }
        }
    }

    public Contact getNewContactFromConsole() {

        String firstName = getValidFirstName();
        String lastName = getValidLastName();
        String number = getValidNumber();
        NumberType type = getTypeToUpdate();

        return new Contact(firstName, lastName, number, type);
    }

    public String getValidFirstName() {

        askName();
        return getLineFromConsole(name -> {
                    boolean isValid = false;
                    try {
                        isValid = ValidationService.firstNameIsValid(name);
                    } catch (ObligatoryFieldIsBlankException | NonValidLengthException e) {
                        System.err.println(e.getMessage());
                        logger.trace("Non-valid first name input: [{}], {}", name, e.getMessage());
                    }
                    return isValid;
                }
        );
    }

    public String getValidLastName() {
        askLastName();

        return getLineFromConsole(lastName -> {
                    boolean isValid = false;
                    try {
                        isValid = ValidationService.lastNameIsValid(lastName);
                    } catch (NonValidLengthException e) {
                        System.err.println(e.getMessage());
                        logger.trace("Non-valid last name input: [{}], {}", lastName, e.getMessage());
                    }
                    return isValid;
                }
        );
    }

    public String getValidNumber() {
        askNumber();

        return getLineFromConsole(number -> {
                    boolean isValid = false;
                    try {
                        isValid = ValidationService.numberIsValid(number);
                    } catch (ObligatoryFieldIsBlankException | NonValidNumberException e) {
                        System.err.println(e.getMessage());
                        logger.trace("Non-valid telephone number input: [{}], {}", number, e.getMessage());
                    }
                    return isValid;
                }
        );
    }

    public boolean getBack() {
        sayMayGetBack();
        while (true) {
            int choice = getIntegerFromConsole();
            if (choice == 0) {
                return true;
            } else {
                sayTryAgain();
            }
        }
    }

}
