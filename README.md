# ConsolePhoneBook
TASK:
Implement simple console menu for application. For example: add contact | update contact | delete contact | select all | log off. 
Navigate throw pages by Users input: "1" leads to "add contact".

All fields must be validated before entering.

All information should be stored in a file.

The user enters first name, last name, phone number and other additional fields into the console, then the date of creation 
of this contact is automatically added.

The user can perform all CRUD operations with the phonebook.

After reading the data from the file, it must be converted to an object with the appropriate fields.

Create custom exceptions for all cases.

Implement logging of all events.
